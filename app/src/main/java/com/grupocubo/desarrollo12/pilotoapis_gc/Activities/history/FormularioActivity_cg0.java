package com.grupocubo.desarrollo12.pilotoapis_gc.Activities.history;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.grupocubo.desarrollo12.pilotoapis_gc.DataBase.AdminSQLiteOpenHelper;
import com.grupocubo.desarrollo12.pilotoapis_gc.R;

import java.util.ArrayList;
import java.util.Arrays;

public class FormularioActivity_cg0 extends AppCompatActivity {


    private Integer cedula;
    private EditText et_cedula;
    private Button buscar;
    private Button btn_nuevaconsulta;
    private TextView textoBusqueda;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_cg0);

        et_cedula = (EditText) findViewById(R.id.et_cedula);
        buscar = (Button) findViewById(R.id.btn_buscar);
        btn_nuevaconsulta = (Button) findViewById(R.id.btn_nuevaconsulta);
        textoBusqueda = (TextView) findViewById(R.id.tv_buscarConsulta);


        btn_nuevaconsulta.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent ListSong = new Intent(FormularioActivity_cg0.this, FormularioActivity_cg1.class);
                ListSong.putExtra("cedula",Integer.parseInt(et_cedula.getText().toString()));
                startActivity(ListSong);
            }

        });


        buscar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                //////////-------------------------

                cedula = Integer.parseInt(et_cedula.getText().toString());

                String [] formulario_cg1 = {"fecha", "hora", "tipo_consulta", "motivo", "hallazgos"};
                String [] formulario_cg2 = {"peso", "talla", "imc", "temperatura", "tension", "FC", "FR", "BAI","SR","SP"};
                String [] formulario_cg3 = {"diabetes", "diabetes_t","hiper", "hiper_t", "cancer", "cancer_t", "epilepsia", "epilepsia_t","quirur","aler", "aler_t","otros","otros_t"};
                String [] formulario_cg4 = {"cabeza", "cardio", "abdomen","extremidades","sistemaner", "sexo","planifica","metodo","tiempo","dias","meses","años"};

                AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(FormularioActivity_cg0.this,"administracion", null, (int) 1);

                SQLiteDatabase bd = admin.getWritableDatabase();

                int bandera=0;


                Cursor fila1 = bd.rawQuery(
                        "select fecha, hora, tipo_consulta, motivo, hallazgos from consultag_1 where cedula=" + cedula, null
                );

                Cursor fila2 = bd.rawQuery(
                        "select peso, talla, imc, temperatura, tension, FC, FR, BAI,SR,SP from consultag_2 where cedula=" + cedula, null
                );
                Cursor fila3 = bd.rawQuery(
                        "select diabetes, diabetes_t,hiper, hiper_t, cancer, cancer_t, epilepsia, epilepsia_t,quirur,aler, aler_t,otros,otros_t from consultag_3 where cedula=" + cedula, null

                );
                Cursor fila4 = bd.rawQuery(
                        "select cabeza, cardio, abdomen,extremidades,sistemaner, sexo,planifica,metodo,tiempo,dias,meses,años from consultag_4 where cedula=" + cedula, null
                );


                ArrayList<String> textoMostrar = new ArrayList<String>();

                int cont=0;

                if (fila1.moveToFirst()) {

                    for (int i=0; i<5; i++)
                    {

                            textoMostrar.add(formulario_cg1[i] + ":  " + fila1.getString(i));
                            textoMostrar.add(System.getProperty ("line.separator"));

                    }

                }


                if (fila2.moveToFirst()) {
                    for (int j=0; j<10; j++)
                    {
                        textoMostrar.add(formulario_cg2[j] + ":  " + String.valueOf(fila2.getInt(j)));
                        textoMostrar.add(System.getProperty ("line.separator"));
                    }
                }

                if (fila3.moveToFirst()) {
                    for (int j=0; j<13; j++)
                    {
                        textoMostrar.add(formulario_cg3[j] + ":  " + String.valueOf(fila3.getInt(j)));
                        textoMostrar.add(System.getProperty ("line.separator"));
                    }
                }

                if (fila4.moveToFirst()) {
                    for (int k=0; k<12; k++)
                    {
                        textoMostrar.add(formulario_cg4[k] + ":  " + fila4.getString(k));
                        textoMostrar.add(System.getProperty ("line.separator"));
                    }
                }
/*
/*
                    for (int k=0; k<13; k++)
                    {
                        textoMostrar.add( fila3.toString() );
                    }



                    for (int i=0; i<12; i++)
                    {
                        textoMostrar.add(  fila4.toString() );
                    }

*/

                bd.close();


                textoBusqueda.setText(Arrays.toString(new ArrayList[]{textoMostrar}).replaceAll("\\[|\\]", ""));

                ///////////-----------------------------------




            }

        });



    }


}
