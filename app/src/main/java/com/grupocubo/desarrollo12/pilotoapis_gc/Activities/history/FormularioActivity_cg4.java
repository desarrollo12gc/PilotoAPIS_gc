package com.grupocubo.desarrollo12.pilotoapis_gc.Activities.history;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.grupocubo.desarrollo12.pilotoapis_gc.Classes.Recursiva;
import com.grupocubo.desarrollo12.pilotoapis_gc.DataBase.AdminSQLiteOpenHelper;
import com.grupocubo.desarrollo12.pilotoapis_gc.R;
import com.grupocubo.desarrollo12.pilotoapis_gc.Widget.OnSwipeTouchListener;

public class FormularioActivity_cg4 extends AppCompatActivity {


    private LinearLayout LinearLayourCG1;
    private Integer cedula;
    private Integer i =0;

    private EditText et_cabeza;
    private EditText et_cardio;
    private EditText et_abdomen;
    private EditText et_extremidades;
    private EditText et_sisner;

    private RadioButton rb_sexo_si;
    private RadioButton rb_sexo_no;
    private RadioButton rb_plan_si;
    private RadioButton rb_plan_no;

    private Spinner spinner_metodo;

    private EditText et_tiempo;

    private RadioButton rb_dias;
    private  RadioButton rb_meses;
    private  RadioButton rb_años;

    private Button btn_editar4;
    private Button btn_volver4;

    private LinearLayout ll_formulario_cg4_2;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_cg4);

        LinearLayourCG1 = (LinearLayout) findViewById(R.id.linearLayout_formulario_cg1);

        LinearLayourCG1.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg4.this) {

            public void onSwipeRight() {
               // Toast.makeText(FormularioActivity_cg4.this, "right", Toast.LENGTH_SHORT).show();
                guardarDB4(LinearLayourCG1);
                Intent ListSong = new Intent(FormularioActivity_cg4.this, FormularioActivity_cg3.class);
                ListSong.putExtra("cedula",cedula);
                startActivity(ListSong);

            }
            public void onSwipeLeft() {
                guardarDB4(LinearLayourCG1);
                //Toast.makeText(FormularioActivity_cg4.this, "left", Toast.LENGTH_SHORT).show();
                Intent ListSong = new Intent(FormularioActivity_cg4.this, FormularioActivity_cg5.class);
                ListSong.putExtra("cedula",cedula);
                startActivity(ListSong);
            }

        });

        ll_formulario_cg4_2 = (LinearLayout) findViewById(R.id.ll_formulario_cg4_2);

        ll_formulario_cg4_2.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg4.this) {

            public void onSwipeRight() {
                //Toast.makeText(FormularioActivity_cg4.this, "right", Toast.LENGTH_SHORT).show();
                guardarDB4(ll_formulario_cg4_2);
                Intent ListSong = new Intent(FormularioActivity_cg4.this, FormularioActivity_cg3.class);
                ListSong.putExtra("cedula",cedula);
                startActivity(ListSong);

            }
            public void onSwipeLeft() {
                guardarDB4(ll_formulario_cg4_2);
                //Toast.makeText(FormularioActivity_cg4.this, "left", Toast.LENGTH_SHORT).show();
                Intent ListSong = new Intent(FormularioActivity_cg4.this, FormularioActivity_cg5.class);
                ListSong.putExtra("cedula",cedula);
                startActivity(ListSong);
            }

        });

         et_cabeza = (EditText) findViewById(R.id.et_cabeza);
         et_cardio = (EditText) findViewById(R.id.et_cardio);
         et_abdomen = (EditText) findViewById(R.id.et_abdomen);
         et_extremidades = (EditText) findViewById(R.id.et_extremidades);
         et_sisner = (EditText) findViewById(R.id.et_sistemaner);

         rb_sexo_si = (RadioButton) findViewById(R.id.rb_sexu_si);
         rb_sexo_no = (RadioButton) findViewById(R.id.rb_sexu_no);
         rb_plan_si = (RadioButton) findViewById(R.id.rb_plani_si);
         rb_plan_no = (RadioButton) findViewById(R.id.rb_plani_no);

         spinner_metodo = (Spinner) findViewById(R.id.spinner_metodos);

         et_tiempo = (EditText) findViewById(R.id.et_uso);

         rb_dias = (RadioButton) findViewById(R.id.rb_uso_dias);
         rb_meses = (RadioButton) findViewById(R.id.rb_uso_meses);
         rb_años = (RadioButton) findViewById(R.id.rb_uso_anios);

         btn_editar4 = (Button) findViewById(R.id.btn_editar4);
         /*-----------------------------------
        Recibir datos del activity anterior
        ///----------------------------------*/

        Intent intent=getIntent();
        Bundle extras =intent.getExtras();
        if (extras != null) {//ver si contiene datos
            Integer cedulaRecibida =(Integer) extras.get("cedula");
            cedula = cedulaRecibida;
           // Toast.makeText(this, "Cedula: " + cedula, Toast.LENGTH_SHORT).show();
            consulta(cedulaRecibida);
        }

        btn_editar4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(i == 0) {
                    btn_editar4.setText("Cancelar");
                    habilitarTodo(true);
                    i++;
                }
                else {
                    btn_editar4.setText("Editar");
                    habilitarTodo(false);
                    i =0;
                }
            }

        });

        btn_volver4 = (Button) findViewById(R.id.btn_volver4);

        btn_volver4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Recursiva re = new Recursiva();
                re.volver(getBaseContext(),cedula);
            }

        });

    }




    public void guardarDB4(View v) {

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,
                "administracion", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        ContentValues registro = new ContentValues();


        registro.put("cedula", cedula);
        registro.put("cabeza",  et_cabeza.getText().toString());
        registro.put("cardio",  et_cardio.getText().toString());
        registro.put("abdomen",  et_abdomen.getText().toString());
        registro.put("extremidades",  et_extremidades.getText().toString());
        registro.put("sistemaner",  et_sisner.getText().toString());

        if(rb_sexo_si.isChecked()==true)
        {
            registro.put("sexo", 1);

        }else
        {
            registro.put("sexo", 0);

        }

        if(rb_plan_si.isChecked()==true)
        {
            registro.put("planifica", 1);
            registro.put("metodo",spinner_metodo.getSelectedItemPosition());
            registro.put("tiempo",Integer.parseInt(et_tiempo.getText().toString()));
            if(rb_dias.isChecked())
            {
                registro.put("dias",1);
                registro.put("meses",0);
                registro.put("años",0);

            }else if(rb_meses.isChecked())
            {
                registro.put("dias",0);
                registro.put("meses",1);
                registro.put("años",0);
            }else  if(rb_años.isChecked())
            {
                registro.put("dias",0);
                registro.put("meses",0);
                registro.put("años",1);
            }

        }else{
            registro.put("planifica", 0);
            registro.put("metodo","no");
            registro.put("tiempo",0);
            registro.put("dias",1);
            registro.put("meses",0);
            registro.put("años",0);
        }

        // Sí existe el usuario para actualizar los datos

        Cursor fila = bd.rawQuery("select cedula from consultag_4T where cedula=" + cedula, null);

        if (fila.moveToFirst()) {
            int cant = bd.update("consultag_4T", registro, "cedula=" + cedula, null);
            Toast.makeText(this, "Actualización satisfactoria", Toast.LENGTH_SHORT).show();


        } else {

            // los inserto en la base de datos
            bd.insert("consultag_4T", null, registro);
            bd.close();
            Toast.makeText(this, "Datos del usuario cargados", Toast.LENGTH_SHORT).show();
        }



        if (v.getId()==R.id.btn_guardar4)
        {
            Recursiva re = new Recursiva();
            re.guardarDB(getBaseContext(),cedula);
        }





            //-------------------------------------------------



        bd.close();

    }


    public void consulta(Integer cedula){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,

                "administracion", null, (int) 1);

        SQLiteDatabase bd = admin.getWritableDatabase();



        Cursor fila = bd.rawQuery("select cabeza, cardio, abdomen,extremidades,sistemaner, sexo,planifica,metodo,tiempo,dias,meses,años from consultag_4T where cedula=" + cedula, null);
        Cursor fila1 = bd.rawQuery("select cabeza, cardio, abdomen,extremidades,sistemaner, sexo,planifica,metodo,tiempo,dias,meses,años from consultag_4 where cedula=" + cedula, null);

        if (fila.moveToFirst()) {

            et_cabeza.setText(fila.getString(0));
            et_cardio.setText(fila.getString(1));
            et_abdomen.setText(fila.getString(2));
            et_extremidades.setText(fila.getString(3));
            et_sisner.setText(fila.getString(4));

            //Toast.makeText(this, fila.getString(7), Toast.LENGTH_SHORT).show();


            if(fila.getInt(5)==1)
            {
                rb_sexo_si.setChecked(true);
            }else
            {
                rb_sexo_no.setChecked(true);
            }

            if(fila.getInt(6)==1)
            {
                rb_plan_si.setChecked(true);

                spinner_metodo.setSelection(fila.getInt(7));
                et_tiempo.setText( String.valueOf( fila.getInt(8) ));

                if(fila.getInt(9) == 1)
                {
                    rb_dias.setChecked(true);
                }
                if(fila.getInt(10) == 1)
                {
                    rb_meses.setChecked(true);
                }
                if(fila.getInt(11) == 1)
                {
                    rb_años.setChecked(true);
                }

            }else
            {
                rb_plan_no.setChecked(true);
                spinner_metodo.setEnabled(false);
                et_tiempo.setText("");
                rb_dias.setChecked(false);
                rb_meses.setChecked(false);
                rb_años.setChecked(false);
            }

            habilitarTodo(false);
            btn_editar4.setEnabled(true);
        } else if (fila1.moveToFirst()) {

            et_cabeza.setText(fila1.getString(0));
            et_cardio.setText(fila1.getString(1));
            et_abdomen.setText(fila1.getString(2));
            et_extremidades.setText(fila1.getString(3));
            et_sisner.setText(fila1.getString(4));

            //Toast.makeText(this, fila.getString(7), Toast.LENGTH_SHORT).show();


            if(fila1.getInt(5)==1)
            {
                rb_sexo_si.setChecked(true);
            }else
            {
                rb_sexo_no.setChecked(true);
            }

            if(fila1.getInt(6)==1)
            {
                rb_plan_si.setChecked(true);

                spinner_metodo.setSelection(fila1.getInt(7));
                et_tiempo.setText( String.valueOf( fila1.getInt(8) ));

                if(fila1.getInt(9) == 1)
                {
                    rb_dias.setChecked(true);
                }
                if(fila1.getInt(10) == 1)
                {
                    rb_meses.setChecked(true);
                }
                if(fila1.getInt(11) == 1)
                {
                    rb_años.setChecked(true);
                }

            }else
            {
                rb_plan_no.setChecked(true);
                spinner_metodo.setEnabled(false);
                et_tiempo.setText("");
                rb_dias.setChecked(false);
                rb_meses.setChecked(false);
                rb_años.setChecked(false);
            }

            habilitarTodo(false);
            btn_editar4.setEnabled(true);
        }

        else
            {
                btn_editar4.setEnabled(false);
                //Toast.makeText(this, "No existe ningún usuario con ese dni", Toast.LENGTH_SHORT).show();

            }

        bd.close();

    }



    public void habilitarTodo(boolean opcion) {


        et_cabeza.setEnabled(opcion);
        et_cardio.setEnabled(opcion);
        et_abdomen.setEnabled(opcion);
        et_extremidades.setEnabled(opcion);
        et_sisner.setEnabled(opcion);

        rb_sexo_si.setEnabled(opcion);
        rb_sexo_no.setEnabled(opcion);
        rb_plan_si.setEnabled(opcion);
        rb_plan_no.setEnabled(opcion);

        spinner_metodo.setEnabled(opcion);

        et_tiempo.setEnabled(opcion);

        rb_dias.setEnabled(opcion);
        rb_meses.setEnabled(opcion);
        rb_años.setEnabled(opcion);

    }

}
