package com.grupocubo.desarrollo12.pilotoapis_gc.Activities.history;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.grupocubo.desarrollo12.pilotoapis_gc.Classes.Recursiva;
import com.grupocubo.desarrollo12.pilotoapis_gc.DataBase.AdminSQLiteOpenHelper;
import com.grupocubo.desarrollo12.pilotoapis_gc.R;
import com.grupocubo.desarrollo12.pilotoapis_gc.Widget.OnSwipeTouchListener;

import java.util.Calendar;

public class FormularioActivity_cg1 extends AppCompatActivity{


    private  Integer cedula;

    private LinearLayout LL_formulario_cg1;
    private TextView fecha;
    private TextView hora;
    private TextView campa;
    private ImageView btn_fecha;
    private EditText hallazgos;
    private Spinner consulta;
    private EditText motivo;
    private Button btn_editar1;
    private Button btn_volver1;



    private LinearLayout t1;
    private LinearLayout t2;
    private LinearLayout t3;
    private LinearLayout t4;
    private LinearLayout t5;
    private LinearLayout t6;
    private LinearLayout t7;
    private LinearLayout t8;
    private LinearLayout t9;
    private LinearLayout t10;
    private LinearLayout t11;
    private LinearLayout t12;


    private static final String CERO = "0";
    private static final String BARRA = "/";

    //Calendario para obtener fecha & hora
    public final Calendar c = Calendar.getInstance();

    //Variables para obtener la fecha
    final int mes = c.get(Calendar.MONTH);
    final int dia = c.get(Calendar.DAY_OF_MONTH);
    final int anio = c.get(Calendar.YEAR);

    private ConstraintLayout pruebaLayout;
    private Integer i =0;

    @SuppressLint("ClickableViewAccessibility")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_cg1);

        LL_formulario_cg1 = (LinearLayout) findViewById(R.id.LL_formulario_cg1);

        LL_formulario_cg1.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg1.this) {

            public void onSwipeRight() {
                overridePendingTransition(R.anim.animation_enter,R.anim.animation_leave);
                Intent ListSong = new Intent(FormularioActivity_cg1.this, FormularioActivity_cg0.class);
                startActivity(ListSong);



            }

            public void onSwipeLeft() {
                //Toast.makeText(FormularioActivity_cg1.this, "left", Toast.LENGTH_SHORT).show();
                overridePendingTransition(R.anim.animation_leave,R.anim.animation_enter);
                guardarDB(LL_formulario_cg1);
                Intent ListSong = new Intent(FormularioActivity_cg1.this, FormularioActivity_cg2.class);
                ListSong.putExtra("cedula",cedula);
                startActivity(ListSong);

            }

        });



        pruebaLayout = (ConstraintLayout) findViewById(R.id.idprueba);

        pruebaLayout.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg1.this) {

            public void onSwipeRight() {
                overridePendingTransition(R.anim.animation_enter,R.anim.animation_leave);
                Intent ListSong = new Intent(FormularioActivity_cg1.this, FormularioActivity_cg0.class);
                startActivity(ListSong);

            }

            public void onSwipeLeft() {
                //Toast.makeText(FormularioActivity_cg1.this, "left", Toast.LENGTH_SHORT).show();
                overridePendingTransition(R.anim.animation_leave,R.anim.animation_enter);
                guardarDB(LL_formulario_cg1);
                Intent ListSong = new Intent(FormularioActivity_cg1.this, FormularioActivity_cg2.class);
                ListSong.putExtra("cedula",cedula);
                startActivity(ListSong);

            }

        });


        //_-----------------------------------------------------------------------------------------------------------

        t1 = (LinearLayout) findViewById(R.id.t1);
        t2 = (LinearLayout) findViewById(R.id.t2);;
        t3 = (LinearLayout) findViewById(R.id.t3);;
        t4 = (LinearLayout) findViewById(R.id.t4);;
        t5 = (LinearLayout) findViewById(R.id.t5);;
        t6 = (LinearLayout) findViewById(R.id.t6);;
        t7 = (LinearLayout) findViewById(R.id.t7);;
        t8 = (LinearLayout) findViewById(R.id.t8);;
        t9 = (LinearLayout) findViewById(R.id.t9);;
        t10 = (LinearLayout) findViewById(R.id.t10);;
        t11 = (LinearLayout) findViewById(R.id.t11);;
        t12 = (LinearLayout) findViewById(R.id.t12);;


        t1.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg1.this) { public void onSwipeRight() { onSwipeRight1();} public void onSwipeLeft() {onSwipeLeft2();}});
        t2.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg1.this) { public void onSwipeRight() { onSwipeRight1();} public void onSwipeLeft() {onSwipeLeft2();}});
        t3.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg1.this) { public void onSwipeRight() { onSwipeRight1();} public void onSwipeLeft() {onSwipeLeft2();}});
        t4.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg1.this) { public void onSwipeRight() { onSwipeRight1();} public void onSwipeLeft() {onSwipeLeft2();}});
        t5.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg1.this) { public void onSwipeRight() { onSwipeRight1();} public void onSwipeLeft() {onSwipeLeft2();}});
        t6.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg1.this) { public void onSwipeRight() { onSwipeRight1();} public void onSwipeLeft() {onSwipeLeft2();}});
        t7.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg1.this) { public void onSwipeRight() { onSwipeRight1();} public void onSwipeLeft() {onSwipeLeft2();}});
        t8.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg1.this) { public void onSwipeRight() { onSwipeRight1();} public void onSwipeLeft() {onSwipeLeft2();}});
        t9.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg1.this) { public void onSwipeRight() { onSwipeRight1();} public void onSwipeLeft() {onSwipeLeft2();}});
        t10.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg1.this) { public void onSwipeRight() { onSwipeRight1();} public void onSwipeLeft() {onSwipeLeft2();}});
        t11.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg1.this) { public void onSwipeRight() { onSwipeRight1();} public void onSwipeLeft() {onSwipeLeft2();}});
        //t12.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg1.this) { public void onSwipeRight() { onSwipeRight();} public void onSwipeLeft() {onSwipeLeft();}});




       fecha = (TextView) findViewById(R.id.tv_fecha);
       hora  = (TextView) findViewById(R.id.tv_hora);
       btn_fecha = (ImageView) findViewById(R.id.btn_fecha);
       campa = (TextView) findViewById(R.id.tv_campaña);
       consulta = (Spinner) findViewById(R.id.spinner_consulta)  ;
       motivo = (EditText) findViewById(R.id.et_motivo_consulta);
       hallazgos = (EditText) findViewById(R.id.et_hallazgos);
       btn_editar1 = (Button) findViewById(R.id.btn_editar1);


        TimePicker timePicker = new TimePicker(this);
        int hora1= timePicker.getCurrentHour();
        int min= timePicker.getCurrentMinute();

        hora.setText(String.valueOf(hora1)+": "+String.valueOf(min));
        campa.setText("C001");

        btn_fecha.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                        obtenerFecha();
            }

        });

         /*-----------------------------------
        Recibir datos del activity anterior
        ///----------------------------------*/

        Intent intent=getIntent();
        Bundle extras =intent.getExtras();
        if (extras != null) {//ver si contiene datos
            Integer cedulaRecibida =(Integer) extras.get("cedula");
            cedula = cedulaRecibida;
            consulta(cedulaRecibida);
        }


        btn_editar1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(i == 0) {
                    btn_editar1.setText("Cancelar");
                    habilitarTodo(true);
                    i++;
                }
                else {
                    btn_editar1.setText("Editar");
                    habilitarTodo(false);
                    i =0;
                }
            }

        });


        btn_volver1 = (Button) findViewById(R.id.btn_volver1);

        btn_volver1.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Recursiva re = new Recursiva();
                re.volver(getBaseContext(),cedula);
            }

        });



    }


    // Damos de alta y actualizamos los usuarios en nuestra aplicación
    public void guardarDB(View v) {

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,
                "administracion", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        String fechaG = fecha.getText().toString();
        String horaG = hora.getText().toString();
        String IDCamapañaG = campa.getText().toString();
        String consultaG = String.valueOf(consulta.getSelectedItemPosition());
        String motivoG = motivo.getText().toString();
        String hallazgosG = hallazgos.getText().toString();

        ContentValues registro = new ContentValues();


        registro.put("cedula", cedula);
        registro.put("fecha", fechaG);
        registro.put("hora", horaG);
        //registro.put("idCam", IDCamapañaG);
        registro.put("tipo_consulta", consultaG);
        registro.put("motivo", motivoG);
        registro.put("hallazgos", hallazgosG);

        int restriccion =0;

        Cursor fila = bd.rawQuery("select cedula from consultag_1T where cedula=" + cedula, null);
        int contradiccion=0;

        if(fila.moveToFirst())
        {
            int cant = bd.update("consultag_1T", registro, "cedula=" + cedula, null);
            Toast.makeText(this, "Actualización satisfactoria", Toast.LENGTH_SHORT).show();
        }else{
            // los inserto en la base de datos
            bd.insert("consultag_1T", null, registro);
            Toast.makeText(this, "Datos del usuario cargados", Toast.LENGTH_SHORT).show();
        }
        bd.close();

        if (v.getId()==R.id.btn_guardar1)
        {

            Recursiva re = new Recursiva();
            restriccion = re.guardarDB(getBaseContext(),cedula);


        }




    }


    public void consulta(Integer cedula){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,

                "administracion", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        Cursor fila = bd.rawQuery("select fecha, hora, tipo_consulta, motivo, hallazgos from consultag_1T where cedula=" + cedula, null);
        Cursor fila1 = bd.rawQuery("select fecha, hora, tipo_consulta, motivo, hallazgos from consultag_1 where cedula=" + cedula, null);

        if (fila.moveToFirst()) {


            if(fila.getString(0)!= null ){
                fecha.setText(fila.getString(0));
            }

            if(fila.getString(1)!= null ){
                hora.setText(fila.getString(1));
            }
//          consulta.setSelection(Integer.parseInt(fila.getString(2)));

            if(fila.getString(3)!= null ){
                motivo.setText(fila.getString(3));
            }

            if(fila.getString(4)!= null ){
                hallazgos.setText(fila.getString(4));
            }

            habilitarTodo(false);
            btn_editar1.setEnabled(true);

        }else if(fila1.moveToFirst())
            {
                if(fila1.getString(0)!= null ){
                    fecha.setText(fila1.getString(0));
                }

                if(fila1.getString(1)!= null ){
                    hora.setText(fila1.getString(1));
                }
//          consulta.setSelection(Integer.parseInt(fila.getString(2)));

                if(fila1.getString(3)!= null ){
                    motivo.setText(fila1.getString(3));
                }

                if(fila1.getString(4)!= null ){
                    hallazgos.setText(fila1.getString(4));
                }

                habilitarTodo(false);
                btn_editar1.setEnabled(true);
            }

            else
            {
            btn_editar1.setEnabled(false);
            //Toast.makeText(this, "No existe ningún usuario con ese dni", Toast.LENGTH_SHORT).show();
            }
        bd.close();

    }


    public void obtenerFecha() {
        DatePickerDialog recogerFecha = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //Esta variable lo que realiza es aumentar en uno el mes ya que comienza desde 0 = enero
                final int mesActual = month + 1;
                //Formateo el día obtenido: antepone el 0 si son menores de 10
                String diaFormateado = (dayOfMonth < 10) ? CERO + String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                //Formateo el mes obtenido: antepone el 0 si son menores de 10
                String mesFormateado = (mesActual < 10) ? CERO + String.valueOf(mesActual) : String.valueOf(mesActual);
                //Muestro la fecha con el formato deseado
                fecha.setText(diaFormateado + BARRA + mesFormateado + BARRA + year);


            }
            //Estos valores deben ir en ese orden, de lo contrario no mostrara la fecha actual
            /**
             *También puede cargar los valores que usted desee
             */
        }, anio, mes, dia);
        //Muestro el widget
        recogerFecha.show();
    }

    public void onRestoreInstanceState(Bundle estado) {

        fecha.setText("fecha");
        hora.setText("hora");
        campa.setText("campaña");
        hallazgos.setText("Hallazgos");
        motivo.setText("motivo");

       // super.onRestoreInstanceState(estado);

    }

    public void onSaveInstanceState(Bundle estado) {


        estado.putString("fecha", String.valueOf(fecha.getText()));
        estado.putString("hora", String.valueOf(hora.getText()));
        estado.putString("campaña", String.valueOf(campa.getText()));
        estado.putString("Hallazgos", String.valueOf(hallazgos.getText()) );
        estado.putString("motivo",  String.valueOf(motivo.getText()));

        super.onSaveInstanceState(estado);

    }

    public void habilitarTodo(boolean opcion){
        fecha.setEnabled(opcion);
        hora.setEnabled(opcion);
        campa.setEnabled(opcion);
        consulta.setEnabled(opcion);
        motivo.setEnabled(opcion);
        hallazgos.setEnabled(opcion);

    }


    public void onSwipeRight1() {
        overridePendingTransition(R.anim.animation_enter,R.anim.animation_leave);
        Intent ListSong = new Intent(FormularioActivity_cg1.this, FormularioActivity_cg0.class);
        startActivity(ListSong);


    }

    public void onSwipeLeft2() {
        overridePendingTransition(R.anim.animation_leave,R.anim.animation_enter);
        guardarDB(LL_formulario_cg1);
        Intent ListSong = new Intent(FormularioActivity_cg1.this, FormularioActivity_cg2.class);
        ListSong.putExtra("cedula",cedula);
        startActivity(ListSong);
       ;
    }



}
