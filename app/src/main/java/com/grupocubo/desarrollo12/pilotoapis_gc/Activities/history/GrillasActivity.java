package com.grupocubo.desarrollo12.pilotoapis_gc.Activities.history;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.grupocubo.desarrollo12.pilotoapis_gc.R;

import java.util.ArrayList;

public class GrillasActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grillas);

        Tabla tabla = new Tabla(this, (TableLayout)findViewById(R.id.tabla));
        tabla.agregarCabecera(R.array.cabecera_tabla);
        for(int i = 0; i < 30; i++)
        {
            ArrayList<String> elementos = new ArrayList<String>();
            elementos.add("Imagen " + i);
            elementos.add("Cedula de ciudadania  " + ", 0]");
            elementos.add("1033812680"+ " ,1]");
            elementos.add("Esteban Moreno Castillo" + ", 2]");
            elementos.add("PRUEBA PRUEBA PRUEBA" + i + ", 3]");
            tabla.agregarFilaTabla(elementos);
        }
        

        Tabla tabla2 = new Tabla(this, (TableLayout) findViewById(R.id.tabla2));
        tabla2.agregarCabecera(R.array.cabecera_tabla2);


        for (int i=0; i<20; i++)
        {
            ArrayList<EstructuraTabla> elementos2 = new ArrayList<EstructuraTabla>();
            ImageButton boton = new ImageButton(this, null);


            EstructuraTabla estru = new EstructuraTabla("Esteban Moreno Castillo " + ", 2]", "1033812680" + " ,1]", boton) ;
            elementos2.add(estru);
            tabla2.agregarFilaTabla2(elementos2);


        }


        for(TableRow a: tabla2.getFilas())
        {
            View boton =  a.getVirtualChildAt(2);
            boton.setOnClickListener((new View.OnClickListener() {
                AlertDialog.Builder contruccion = new AlertDialog.Builder(GrillasActivity.this);
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder contruccion = new AlertDialog.Builder(GrillasActivity.this);
                    contruccion.setTitle("Información faltante");
                    contruccion.setMessage(
                              "1. Imagen "
                            + "2. Cedula de ciudadania:  "
                            + "3. 1033812680 "
                            + "4. Esteban Moreno Castillo "
                            + "5. PRUEBA PRUEBA PRUEBA");

                    AlertDialog dialog = contruccion.create();
                    dialog.show();

                }

            }));

        }

    }

}


class Tabla {

    private TableLayout tabla; // Layout donde se pintará la tabla

    public ArrayList<TableRow> getFilas() {
        return filas;
    }

    public void setFilas(ArrayList<TableRow> filas) {
        this.filas = filas;
    }

    private ArrayList<TableRow> filas; // Array de las filas de la tabla
    private Activity actividad;
    private Resources rs;
    private int FILAS, COLUMNAS; // Filas y columnas de nuestra tabla


    public Tabla(Activity actividad, TableLayout tabla)
    {
        this.actividad = actividad;
        this.tabla = tabla;
        rs = this.actividad.getResources();
        FILAS = COLUMNAS = 0;
        filas = new ArrayList<TableRow>();
    }

    public void agregarCabecera(int recursocabecera)
    {
        TableRow.LayoutParams layoutCelda;
        TableRow fila = new TableRow(actividad);
        TableRow.LayoutParams layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        fila.setLayoutParams(layoutFila);

        String[] arraycabecera = rs.getStringArray(recursocabecera);
        COLUMNAS = arraycabecera.length;

        for(int i = 0; i < arraycabecera.length; i++)
        {
            TextView texto = new TextView(actividad);
            layoutCelda = new TableRow.LayoutParams(obtenerAnchoPixelesTexto(arraycabecera[i],40), TableRow.LayoutParams.WRAP_CONTENT);
            texto.setText(arraycabecera[i]);
            texto.setGravity(Gravity.CENTER_HORIZONTAL);
            //texto.setTextAppearance(actividad, );
            //texto.setBackgroundResource(R.drawable.tabla_celda_cabecera);
            texto.setLayoutParams(layoutCelda);

            fila.addView(texto);
        }

        tabla.addView(fila);
        filas.add(fila);

        FILAS++;
    }


    public void agregarFilaTabla(ArrayList<String> elementos)
    {
        TableRow.LayoutParams layoutCelda;
        TableRow.LayoutParams layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT);
        TableRow fila = new TableRow(actividad);
        fila.setLayoutParams(layoutFila);

        for(int i = 0; i< elementos.size(); i++)
        {
            TextView texto = new TextView(actividad);
            texto.setText(String.valueOf(elementos.get(i)));
            texto.setGravity(Gravity.CENTER_HORIZONTAL);
            //texto.setTextAppearance(actividad, R.style.estilo_celda);
            //texto.setBackgroundResource(R.drawable.tabla_celda);
            layoutCelda = new TableRow.LayoutParams(obtenerAnchoPixelesTexto(texto.getText().toString(),30), TableRow.LayoutParams.WRAP_CONTENT);
            texto.setLayoutParams(layoutCelda);

            fila.addView(texto);
        }

        tabla.addView(fila);
        filas.add(fila);

        FILAS++;
    }


    public void agregarFilaTabla2(ArrayList<EstructuraTabla> elementos)
    {
        TableRow.LayoutParams layoutCelda;
        TableRow.LayoutParams layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT);
        TableRow fila = new TableRow(actividad);
        fila.setLayoutParams(layoutFila);

        for(int i = 0; i< elementos.size(); i++)
        {
            TextView texto = new TextView(actividad);
            TextView texto2 = new TextView(actividad);
            ImageButton boton = new ImageButton(actividad);

            for (EstructuraTabla e:elementos)
            {
                texto.setText(String.valueOf(e.getNombre()));
                texto.setGravity(Gravity.CENTER_HORIZONTAL);
                layoutCelda = new TableRow.LayoutParams(obtenerAnchoPixelesTexto(texto.getText().toString(),40), TableRow.LayoutParams.WRAP_CONTENT);
                texto.setLayoutParams(layoutCelda);
                fila.addView(texto);

                texto2.setText(String.valueOf(e.getTipoDoc()));
                texto2.setGravity(Gravity.CENTER_HORIZONTAL);
                layoutCelda = new TableRow.LayoutParams(obtenerAnchoPixelesTexto(texto2.getText().toString(),40), TableRow.LayoutParams.WRAP_CONTENT);
                texto.setLayoutParams(layoutCelda);
                fila.addView(texto2);

                layoutCelda = new TableRow.LayoutParams(obtenerAnchoPixelesTexto("0",40), TableRow.LayoutParams.WRAP_CONTENT);
                boton.setLayoutParams(layoutCelda);
                fila.addView(boton);

            }

            //texto.setTextAppearance(actividad, R.style.estilo_celda);
            //texto.setBackgroundResource(R.drawable.tabla_celda);

        }

        tabla.addView(fila);
        filas.add(fila);

        FILAS++;
    }


    private int obtenerAnchoPixelesTexto(String texto, int numero)
    {
        Paint p = new Paint();
        Rect bounds = new Rect();
        p.setTextSize(numero);
        p.getTextBounds(texto, 0, texto.length(), bounds);
        return bounds.width();
    }

}

class EstructuraTabla{

    private String nombre;
    private String tipoDoc;
    private ImageButton boton;

    public EstructuraTabla(String nombre, String tipoDoc, ImageButton boton) {
        this.nombre = nombre;
        this.tipoDoc = tipoDoc;
        this.boton = boton;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public ImageButton getBoton() {
        return boton;
    }

    public void setBoton(ImageButton boton) {
        this.boton = boton;
    }


}

