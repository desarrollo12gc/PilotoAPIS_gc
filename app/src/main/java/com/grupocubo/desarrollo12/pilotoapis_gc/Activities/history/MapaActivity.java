package com.grupocubo.desarrollo12.pilotoapis_gc.Activities.history;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.grupocubo.desarrollo12.pilotoapis_gc.R;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;

public class MapaActivity extends AppCompatActivity {

    private MapView myOpenMapView;
    private MapController myMapController;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);

        GeoPoint madrid = new GeoPoint(40.416775, -3.70379);

        myOpenMapView = (MapView) findViewById(R.id.openmapview);
        myOpenMapView.setBuiltInZoomControls(true);

        myMapController = (MapController) myOpenMapView.getController();
        myMapController.setCenter(madrid);
        myMapController.setZoom(6);

        myOpenMapView.setMultiTouchControls(true);
    }
}