package com.grupocubo.desarrollo12.pilotoapis_gc.Activities.history;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.grupocubo.desarrollo12.pilotoapis_gc.Classes.Recursiva;
import com.grupocubo.desarrollo12.pilotoapis_gc.DataBase.AdminSQLiteOpenHelper;
import com.grupocubo.desarrollo12.pilotoapis_gc.R;
import com.grupocubo.desarrollo12.pilotoapis_gc.Widget.OnSwipeTouchListener;

public class FormularioActivity_cg2 extends AppCompatActivity {



    private LinearLayout LL_formulario_cg2;
    private Integer cedula;

    private EditText et_peso;
    private EditText et_talla;
    private EditText et_IMC;
    private EditText et_temp;
    private EditText et_tension;
    private EditText et_FC;
    private EditText et_FR;

    private RadioButton rb_BAI_si;
    private RadioButton rb_BAI_no;
    private RadioButton rb_SR_si;
    private RadioButton rb_SR_no;
    private RadioButton rb_SP_si;
    private RadioButton rb_SP_no;

    private Button btn_guardar2;
    private Button btn_editar2;

    private ConstraintLayout cl_formulario2;

    private Integer i =0;
    private Button btn_volver2;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_cg2);

        et_peso = findViewById(R.id.et_peso);
        et_talla = findViewById(R.id.et_talla);
        et_IMC = findViewById(R.id.et_IMC);
        et_temp = findViewById(R.id.et_temperatura);
        et_tension = findViewById(R.id.et_tension);
        et_FC = findViewById(R.id.et_latidos);
        et_FR = findViewById(R.id.et_FR);
        rb_BAI_si = findViewById(R.id.rb_BAI_si);
        rb_BAI_no= findViewById(R.id.rb_BAI_no);;
        rb_SR_si= findViewById(R.id.rb_SR_si);;
        rb_SR_no= findViewById(R.id.rb_SR_no);;
        rb_SP_si= findViewById(R.id.rb_SP_si);;
        rb_SP_no= findViewById(R.id.rb_SP_no);;
        btn_guardar2 = findViewById(R.id.btn_guardar2);
        btn_editar2 = findViewById(R.id.btn_editar2);

        cl_formulario2 = findViewById(R.id.cl_formulario_cg2);


        cl_formulario2.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg2.this) {

            public void onSwipeRight() {
                //Toast.makeText(FormularioActivity_cg2.this, "right", Toast.LENGTH_SHORT).show();
                guardarDB2(cl_formulario2);
                Intent ListSong = new Intent(FormularioActivity_cg2.this, FormularioActivity_cg1.class);
                ListSong.putExtra("cedula",cedula);
                startActivity(ListSong);

            }
            public void onSwipeLeft() {
                //Toast.makeText(FormularioActivity_cg2.this, "left", Toast.LENGTH_SHORT).show();
                guardarDB2(cl_formulario2);
                Intent ListSong = new Intent(FormularioActivity_cg2.this, FormularioActivity_cg3.class);
                ListSong.putExtra("cedula",cedula);
                startActivity(ListSong);

            }


        });

        LL_formulario_cg2 = (LinearLayout) findViewById(R.id.LL_formulario_cg2);

    LL_formulario_cg2.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg2.this) {

        public void onSwipeRight() {
            //Toast.makeText(FormularioActivity_cg2.this, "right", Toast.LENGTH_SHORT).show();
            guardarDB2(LL_formulario_cg2);
            Intent ListSong = new Intent(FormularioActivity_cg2.this, FormularioActivity_cg1.class);
            ListSong.putExtra("cedula",cedula);
            startActivity(ListSong);

        }
        public void onSwipeLeft() {
            //Toast.makeText(FormularioActivity_cg2.this, "left", Toast.LENGTH_SHORT).show();
            guardarDB2(LL_formulario_cg2);
            Intent ListSong = new Intent(FormularioActivity_cg2.this, FormularioActivity_cg3.class);
            ListSong.putExtra("cedula",cedula);
            startActivity(ListSong);

        }


    });


      /*-----------------------------------
        Recibir datos del activity anterior
        ///----------------------------------*/

        Intent intent=getIntent();
        Bundle extras =intent.getExtras();
        if (extras != null) {//ver si contiene datos
            Integer cedulaRecibida =(Integer) extras.get("cedula");
            cedula = cedulaRecibida;
                consulta(cedulaRecibida);
        }

        btn_editar2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(i == 0) {
                    btn_editar2.setText("Cancelar");
                    habilitarTodo(true);
                    i++;
                }
                else {
                    btn_editar2.setText("Editar");
                    habilitarTodo(false);
                    i =0;
                }
            }

        });

        btn_volver2 = (Button) findViewById(R.id.btn_volver2);

        btn_volver2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Recursiva re = new Recursiva();
                re.volver(getBaseContext(),cedula);
            }

        });

    }

    public void guardarDB2(View v) {

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,
                "administracion", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();



        ContentValues registro = new ContentValues();


        registro.put("cedula", cedula);
        registro.put("peso", Integer.parseInt( retornar(et_peso) ));
        registro.put("talla",Integer.parseInt( retornar(et_talla)));
        registro.put("imc", Integer.parseInt( retornar(et_IMC)));
        registro.put("temperatura", Integer.parseInt( retornar(et_temp)));
        registro.put("tension",Integer.parseInt( retornar(et_tension)));
        registro.put("FC", Integer.parseInt(  retornar(et_FC) ));
        registro.put("FR", Integer.parseInt( retornar(et_FR)));

        if(rb_BAI_si.isChecked()==true)
        {
            registro.put("BAI", 1);
        }else
        {
            registro.put("BAI", 0);
        }

        if(rb_SR_si.isChecked()==true)
        {
            registro.put("SR", 1);
        }else{
            registro.put("BAI", 0);
        }

        if(rb_SP_si.isChecked()==true)
        {
            registro.put("SP", 1);
        }else{
            registro.put("BAI", 0);
        }

        // Sí existe el usuario para actualizar los datos

        Cursor fila = bd.rawQuery("select cedula from consultag_2T where cedula=" + cedula, null);

        if (fila.moveToFirst()) {
            int cant = bd.update("consultag_2T", registro, "cedula=" + cedula, null);
            Toast.makeText(this, "Actualización satisfactoria", Toast.LENGTH_SHORT).show();


        } else {

            // los inserto en la base de datos
            bd.insert("consultag_2T", null, registro);
            bd.close();
            Toast.makeText(this, "Datos del usuario cargados", Toast.LENGTH_SHORT).show();
        }

        if (v.getId()==R.id.btn_guardar2)
        {
            Recursiva re = new Recursiva();
            re.guardarDB(getBaseContext(),cedula);

        }



        //-------------------------------------------------
        bd.close();




    }


    public void consulta(Integer cedula){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,

                "administracion", null, (int) 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        Cursor fila = bd.rawQuery("select peso, talla, imc, temperatura, tension, FC, FR, BAI,SR,SP from consultag_2T where cedula=" + cedula, null);
        Cursor fila1 = bd.rawQuery("select peso, talla, imc, temperatura, tension, FC, FR, BAI,SR,SP from consultag_2 where cedula=" + cedula, null);

        if (fila.moveToFirst()) {

            et_peso.setText(fila.getString(0));
            et_talla.setText(fila.getString(1));
            et_IMC.setText(fila.getString(2));
            et_temp.setText(fila.getString(3));
            et_tension .setText(fila.getString(4));
            et_FC.setText(fila.getString(5));
            et_FR.setText(fila.getString(6));

            //Toast.makeText(this, fila.getString(7), Toast.LENGTH_SHORT).show();


            if(fila.getInt(7)==1)
            {
                rb_BAI_si.setChecked(true);
            }else
            {
                rb_BAI_no.setChecked(true);
            }

            if(fila.getInt(8)==1)
            {
                rb_SR_si.setChecked(true);
            }else
            {
                rb_SR_no.setChecked(true);
            }

            if(fila.getInt(9)==1)
            {
                rb_SP_si.setChecked(true);
            }else
            {
                rb_SP_no.setChecked(true);
            }

            habilitarTodo(false);
            btn_editar2.setEnabled(true);

        }else if(fila1.moveToFirst()){


            et_peso.setText(fila1.getString(0));
            et_talla.setText(fila1.getString(1));
            et_IMC.setText(fila1.getString(2));
            et_temp.setText(fila1.getString(3));
            et_tension .setText(fila1.getString(4));
            et_FC.setText(fila1.getString(5));
            et_FR.setText(fila1.getString(6));

            //Toast.makeText(this, fila.getString(7), Toast.LENGTH_SHORT).show();


            if(fila1.getInt(7)==1)
            {
                rb_BAI_si.setChecked(true);
            }else
            {
                rb_BAI_no.setChecked(true);
            }

            if(fila1.getInt(8)==1)
            {
                rb_SR_si.setChecked(true);
            }else
            {
                rb_SR_no.setChecked(true);
            }

            if(fila1.getInt(9)==1)
            {
                rb_SP_si.setChecked(true);
            }else
            {
                rb_SP_no.setChecked(true);
            }

            habilitarTodo(false);
            btn_editar2.setEnabled(true);


        }
        else{
            btn_editar2.setEnabled(false);
            //Toast.makeText(this, "No existe ningún usuario con ese dni", Toast.LENGTH_SHORT).show();
        }


        bd.close();

    }

    public String retornar(EditText texto)
    {
        if(texto.getText().toString().equalsIgnoreCase(""))
        {
            return"-1";
        }
        else
        {
        return   texto.getText().toString();
        }
    }



    public void habilitarTodo(boolean opcion) {

        et_peso.setEnabled(opcion);
        et_talla.setEnabled(opcion);
        et_IMC.setEnabled(opcion);
        et_temp.setEnabled(opcion);
        et_tension.setEnabled(opcion);
        et_FC.setEnabled(opcion);
        et_FR.setEnabled(opcion);

        rb_BAI_si.setEnabled(opcion);
        rb_BAI_no.setEnabled(opcion);
        rb_SR_si.setEnabled(opcion);
        rb_SR_no.setEnabled(opcion);
        rb_SP_si.setEnabled(opcion);
        rb_SP_no.setEnabled(opcion);

    }



}
