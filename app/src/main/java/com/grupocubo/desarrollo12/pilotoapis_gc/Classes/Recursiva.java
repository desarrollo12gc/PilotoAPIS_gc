package com.grupocubo.desarrollo12.pilotoapis_gc.Classes;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.grupocubo.desarrollo12.pilotoapis_gc.Activities.history.FormularioActivity_cg0;
import com.grupocubo.desarrollo12.pilotoapis_gc.Activities.history.FormularioActivity_cg1;
import com.grupocubo.desarrollo12.pilotoapis_gc.Activities.history.FormularioActivity_cg2;
import com.grupocubo.desarrollo12.pilotoapis_gc.Activities.history.FormularioActivity_cg3;
import com.grupocubo.desarrollo12.pilotoapis_gc.Activities.history.FormularioActivity_cg4;
import com.grupocubo.desarrollo12.pilotoapis_gc.DataBase.AdminSQLiteOpenHelper;

public class Recursiva  {


    public int guardarDB(Context c, int cedula)
    {


        /*
        Guardar Todos los datos en las tablas oficiales
        */



        int valido=0;

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(c,"administracion", null, (int) 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        String [] formulario_cg1 = new String[]{"fecha", "hora", "tipo_consulta", "motivo", "hallazgos"};
        String [] formulario_cg2 = new String[]{"peso", "talla", "imc", "temperatura", "tension", "FC", "FR", "BAI","SR","SP"};
        String [] formulario_cg3 = new String[]{"diabetes", "diabetes_t","hiper", "hiper_t", "cancer", "cancer_t", "epilepsia", "epilepsia_t","quirur","aler", "aler_t","otros","otros_t"};
        String [] formulario_cg4 = new String[]{"cabeza", "cardio", "abdomen","extremidades","sistemaner", "sexo","planifica","metodo","tiempo","dias","meses","años"};




        ///////---------GUARDAR DATOS DE TEMPORALES EN LA  TABLA VERDADERA---------------\\\\\\

        Cursor fila01 = bd.rawQuery(
                "select fecha, hora, tipo_consulta, motivo, hallazgos from consultag_1 where cedula=" + cedula, null
        );

        Cursor fila02 = bd.rawQuery(
                "select peso, talla, imc, temperatura, tension, FC, FR, BAI,SR,SP from consultag_2 where cedula=" + cedula, null
        );
        Cursor fila03 = bd.rawQuery(
                "select diabetes, diabetes_t,hiper, hiper_t, cancer, cancer_t, epilepsia, epilepsia_t,quirur,aler, aler_t,otros,otros_t from consultag_3 where cedula=" + cedula, null

        );
        Cursor fila04 = bd.rawQuery(
                "select cabeza, cardio, abdomen,extremidades,sistemaner, sexo,planifica,metodo,tiempo,dias,meses,años from consultag_4 where cedula=" + cedula, null
        );

        if(!fila01.moveToFirst())
        {
            bd.execSQL("INSERT INTO consultag_1 SELECT * FROM consultag_1T where cedula=" + cedula);
        }
        else {
            bd.execSQL("delete from consultag_1 where cedula=" + cedula);
            bd.execSQL("INSERT INTO consultag_1 SELECT * FROM consultag_1T where cedula=" + cedula);
        }
        if(!fila02.moveToFirst())
        {
            bd.execSQL("INSERT INTO consultag_2 SELECT * FROM consultag_2T where cedula=" + cedula );
        }else{
            bd.execSQL("delete from  consultag_2 where cedula=" + cedula);
            bd.execSQL("INSERT INTO consultag_2 SELECT * FROM consultag_2T where cedula=" + cedula);
        }
        if(!fila03.moveToFirst())
        {
            bd.execSQL("INSERT INTO consultag_3 SELECT * FROM consultag_3T where cedula=" + cedula);
        }else{
            bd.execSQL("delete from  consultag_3 where cedula=" + cedula);
            bd.execSQL("INSERT INTO consultag_3 SELECT * FROM consultag_3T where cedula=" + cedula);
        }
        if(!fila04.moveToFirst())
        {
            bd.execSQL("INSERT INTO consultag_4 SELECT * FROM consultag_4T where cedula=" + cedula);
        }else{
            bd.execSQL("delete from  consultag_4 where cedula=" + cedula);
            bd.execSQL("INSERT INTO consultag_4 SELECT * FROM consultag_4T where cedula=" + cedula);
        }


        ///////---------------------------------------------------------------------------\\\\\\


        int bandera=0;

        Cursor fila1 = bd.rawQuery(
                "select fecha, hora, tipo_consulta, motivo, hallazgos from consultag_1 where cedula=" + cedula, null
        );

        Cursor fila2 = bd.rawQuery(
                "select peso, talla, imc, temperatura, tension, FC, FR, BAI,SR,SP from consultag_2 where cedula=" + cedula, null
        );
        Cursor fila3 = bd.rawQuery(
                "select diabetes, diabetes_t,hiper, hiper_t, cancer, cancer_t, epilepsia, epilepsia_t,quirur,aler, aler_t,otros,otros_t from consultag_3 where cedula=" + cedula, null

        );
        Cursor fila4 = bd.rawQuery(
                "select cabeza, cardio, abdomen,extremidades,sistemaner, sexo,planifica,metodo,tiempo,dias,meses,años from consultag_4 where cedula=" + cedula, null
        );




        if (fila1.moveToFirst()) {

            int cont=0;

            for (int i=0; i<5; i++)
            {
                if(fila1.getString(i).trim().length()==0)
                {
                    Toast.makeText(c, "Hace falta por llenar: " + formulario_cg1[i], Toast.LENGTH_LONG).show();
                    cont++;
                    valido++;
                }


            }

            if (cont!=0)
            {

                Intent ListSong = new Intent(c, FormularioActivity_cg1.class);
                ListSong.putExtra("cedula",cedula);
                ListSong.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                c.startActivity(ListSong);

            }

        }


        if (fila2.moveToFirst()) {


            int cont=0;

            for (int j=0; j<10; j++)
            {

                if(fila2.getInt(j) == -1 )
                {

                    Toast.makeText(c, "Hace falta por llenar: " + formulario_cg2[j], Toast.LENGTH_LONG).show();
                    cont++;
                    valido++;
                }


            }

            if(cont!=0)
            {

                Intent ListSong = new Intent(c, FormularioActivity_cg2.class);
                ListSong.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ListSong.putExtra("cedula",cedula);
                c.startActivity(ListSong);

            }
        }


        if (fila3.moveToFirst()) {

            int cont=0;

            for (int i=0; i<13; i++)
            {
                if(fila3.getString(i).trim().length()==0 )
                {
                    Toast.makeText(c, "Hace falta por llenar: " + formulario_cg3[i], Toast.LENGTH_LONG).show();
                    cont++;
                    valido++;
                }


            }

            if(cont!=0)
            {

                Intent ListSong = new Intent(c, FormularioActivity_cg3.class);
                ListSong.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ListSong.putExtra("cedula",cedula);
                c.startActivity(ListSong);

            }

        }

        if (fila4.moveToFirst()) {

            int cont=0;

            for (int i=0; i<12; i++)
            {
                if(fila4.getString(i).trim().length()==0 )
                {
                    Toast.makeText(c, "Hace falta por llenar: " + formulario_cg4[i], Toast.LENGTH_LONG).show();
                    cont++;
                    valido++;
                }


            }

            if(cont!=0)
            {

                Intent ListSong = new Intent(c, FormularioActivity_cg4.class);
                ListSong.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ListSong.putExtra("cedula",cedula);
                c.startActivity(ListSong);

            }

        }

        bd.close();

        if (valido==0)
        {
            volver(c,cedula);
        }

        return valido;

    }




    public void volver(Context c, int cedula)
    {
        /*
        Limpia todos los temporales de la DB creados
         */

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(c,"administracion", null, (int) 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        Toast.makeText(c, "Entró", Toast.LENGTH_SHORT);
        bd.execSQL("delete from consultag_1T where cedula=" + cedula);
        bd.execSQL("delete from consultag_2T where cedula=" + cedula);
        bd.execSQL("delete from consultag_3T where cedula=" + cedula);
        bd.execSQL("delete from consultag_4T where cedula=" + cedula);
        Intent ListSong = new Intent(c, FormularioActivity_cg0.class);
        ListSong.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        c.startActivity(ListSong);
    }

}
