package com.grupocubo.desarrollo12.pilotoapis_gc.Activities.history;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.grupocubo.desarrollo12.pilotoapis_gc.Classes.Recursiva;
import com.grupocubo.desarrollo12.pilotoapis_gc.DataBase.AdminSQLiteOpenHelper;
import com.grupocubo.desarrollo12.pilotoapis_gc.R;
import com.grupocubo.desarrollo12.pilotoapis_gc.Widget.OnSwipeTouchListener;

public class FormularioActivity_cg3 extends AppCompatActivity{

    private LinearLayout lL_antefa;
    private LinearLayout lL_anteper;

    private RadioGroup rg_diabetes;
    private RadioButton rb_diabetes_si;
    private RadioButton rb_diabetes_no;
    private EditText et_diabetes;

    private RadioGroup rg_hiper;
    private RadioButton rb_hiper_si;
    private RadioButton rb_hiper_no;
    private EditText et_hiper;

    private RadioGroup rg_cancer;
    private RadioButton rb_cancer_si;
    private RadioButton rb_cancer_no;
    private EditText et_cancer;

    private RadioGroup rg_epilepsia;
    private RadioButton rb_epilepsia_si;
    private RadioButton rb_epilepsia_no;
    private EditText et_epilepsia;

    private RadioGroup rg_quirur;
    private RadioButton rb_quirur_si;
    private RadioButton rb_quirur_no;
    private EditText et_quirur;

    private RadioGroup rg_alergias;
    private RadioButton rb_alergias_si;
    private RadioButton rb_alergias_no;
    private EditText et_alergias;

    private RadioGroup rg_otros;
    private RadioButton rb_otros_si;
    private RadioButton rb_otros_no;
    private EditText et_otros;

    private LinearLayout ll_formulario_cg3_2;

    private Button btn_editar3;


    private Integer cedula;
    private Integer i =0;

    private Button btn_volver3;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_cg3);



        lL_antefa = (LinearLayout) findViewById(R.id.LL_AnteFa);
        lL_anteper = (LinearLayout) findViewById(R.id.LL_AntePer);
        ll_formulario_cg3_2 = (LinearLayout) findViewById(R.id.ll_formulario_cg3);

        lL_antefa.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg3.this) {

            public void onSwipeRight() {
                //Toast.makeText(FormularioActivity_cg3.this, "right", Toast.LENGTH_SHORT).show();
                guardarDB3(lL_antefa);
                Intent ListSong = new Intent(FormularioActivity_cg3.this, FormularioActivity_cg2.class);
                ListSong.putExtra("cedula",cedula);
                startActivity(ListSong);

            }
            public void onSwipeLeft() {
                //Toast.makeText(FormularioActivity_cg3.this, "left", Toast.LENGTH_SHORT).show();
                guardarDB3(lL_antefa);
                Intent ListSong = new Intent(FormularioActivity_cg3.this, FormularioActivity_cg4.class);
                ListSong.putExtra("cedula",cedula);
                startActivity(ListSong);

            }


        });

        lL_anteper.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg3.this) {
            /*public void onSwipeTop() {
                Toast.makeText(FormularioActivity_cg3.this, "top", Toast.LENGTH_SHORT).show();
            }*/
            public void onSwipeRight() {
                guardarDB3(lL_anteper);
                Intent ListSong = new Intent(FormularioActivity_cg3.this, FormularioActivity_cg2.class);
                ListSong.putExtra("cedula",cedula);
                startActivity(ListSong);
            }
            public void onSwipeLeft() {
                //Toast.makeText(FormularioActivity_cg3.this, "left", Toast.LENGTH_SHORT).show();
                guardarDB3(lL_anteper);
                Intent ListSong = new Intent(FormularioActivity_cg3.this, FormularioActivity_cg4.class);
                ListSong.putExtra("cedula",cedula);
                startActivity(ListSong);

            }
            /*public void onSwipeBottom() {
                Toast.makeText(FormularioActivity_cg3.this, "bottom", Toast.LENGTH_SHORT).show();
            }*/

        });

        ll_formulario_cg3_2.setOnTouchListener(new OnSwipeTouchListener(FormularioActivity_cg3.this) {
            /*public void onSwipeTop() {
                Toast.makeText(FormularioActivity_cg3.this, "top", Toast.LENGTH_SHORT).show();
            }*/
            public void onSwipeRight() {
                guardarDB3(ll_formulario_cg3_2);
                Intent ListSong = new Intent(FormularioActivity_cg3.this, FormularioActivity_cg2.class);
                ListSong.putExtra("cedula",cedula);
                startActivity(ListSong);
            }
            public void onSwipeLeft() {
               // Toast.makeText(FormularioActivity_cg3.this, "left", Toast.LENGTH_SHORT).show();
                guardarDB3(ll_formulario_cg3_2);
                Intent ListSong = new Intent(FormularioActivity_cg3.this, FormularioActivity_cg4.class);
                ListSong.putExtra("cedula",cedula);
                startActivity(ListSong);

            }
            /*public void onSwipeBottom() {
                Toast.makeText(FormularioActivity_cg3.this, "bottom", Toast.LENGTH_SHORT).show();
            }*/

        });

        rg_diabetes    = (RadioGroup) findViewById(R.id.rg_diabetes);
        rb_diabetes_si = (RadioButton) findViewById(R.id.rb_diabetes_si);
        rb_diabetes_no = (RadioButton) findViewById(R.id.rb_diabetes_no);
        et_diabetes    = (EditText) findViewById(R.id.et_diabetes);
        //et_diabetes.setEnabled(false);
        et_diabetes.setVisibility(View.INVISIBLE);

        rg_hiper    = (RadioGroup) findViewById(R.id.rg_hiper);
        rb_hiper_si = (RadioButton) findViewById(R.id.rb_hiper_si);
        rb_hiper_no = (RadioButton) findViewById(R.id.rb_hiper_no);
        et_hiper    = (EditText) findViewById(R.id.et_hiper);
        et_hiper.setVisibility(View.INVISIBLE);

        rg_cancer   = (RadioGroup) findViewById(R.id.rg_cancer);
        rb_cancer_si = (RadioButton) findViewById(R.id.rb_cancer_si);
        rb_cancer_no = (RadioButton) findViewById(R.id.rb_cancer_no);
        et_cancer    = (EditText) findViewById(R.id.et_cancer);
        et_cancer.setVisibility(View.INVISIBLE);

        rg_epilepsia = (RadioGroup) findViewById(R.id.rg_epilepsia);
        rb_epilepsia_si = (RadioButton) findViewById(R.id.rb_epilepsia_si);
        rb_epilepsia_no= (RadioButton) findViewById(R.id.rb_epilepsia_no);
        et_epilepsia    = (EditText) findViewById(R.id.et_epilepsia);
        et_epilepsia.setVisibility(View.INVISIBLE);

        rg_quirur = (RadioGroup) findViewById(R.id.rg_quirur);
        rb_quirur_si = (RadioButton) findViewById(R.id.rb_quirur_si);
        rb_quirur_no = (RadioButton) findViewById(R.id.rb_quirur_no);
        et_quirur    = (EditText) findViewById(R.id.et_quirur);
        et_quirur.setVisibility(View.INVISIBLE);

        rg_alergias = (RadioGroup) findViewById(R.id.rg_alergias);
        rb_alergias_si = (RadioButton) findViewById(R.id.rb_alergias_si);
        rb_alergias_no= (RadioButton) findViewById(R.id.rb_alergias_no);
        et_alergias    = (EditText) findViewById(R.id.et_alergias);
        et_alergias.setVisibility(View.INVISIBLE);

        rg_otros = (RadioGroup) findViewById(R.id.rg_otros);
        rb_otros_si = (RadioButton) findViewById(R.id.rb_otros_si);
        rb_otros_no =  (RadioButton) findViewById(R.id.rb_otros_no);
        et_otros    = (EditText) findViewById(R.id.et_otros);
        et_otros.setVisibility(View.INVISIBLE);

        btn_editar3 = (Button) findViewById(R.id.btn_editar3);


        activarEdittext(rg_diabetes,rb_diabetes_si,rb_diabetes_no,et_diabetes);
        activarEdittext(rg_hiper, rb_hiper_si, rb_hiper_no, et_hiper);
        activarEdittext(rg_cancer, rb_cancer_si, rb_cancer_no, et_cancer);
        activarEdittext(rg_epilepsia, rb_epilepsia_si, rb_epilepsia_no, et_epilepsia);
        activarEdittext(rg_quirur, rb_quirur_si, rb_quirur_no, et_quirur);
        activarEdittext(rg_alergias, rb_alergias_si, rb_alergias_no, et_alergias);
        activarEdittext(rg_otros, rb_otros_si, rb_otros_no, et_otros);

         /*-----------------------------------
        Recibir datos del activity anterior
        ///----------------------------------*/

        Intent intent=getIntent();
        Bundle extras =intent.getExtras();
        if (extras != null) {//ver si contiene datos
            Integer cedulaRecibida =(Integer) extras.get("cedula");
            cedula = cedulaRecibida;
            consulta(cedulaRecibida);
        }



        btn_editar3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(i == 0) {
                    btn_editar3.setText("Cancelar");
                    habilitarTodo(true);
                    i++;
                }
                else {
                    btn_editar3.setText("Editar");
                    habilitarTodo(false);
                    i =0;
                }
            }

        });

        btn_volver3 = (Button) findViewById(R.id.btn_volver3);

        btn_volver3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Recursiva re = new Recursiva();
                re.volver(getBaseContext(),cedula);
            }

        });

    }



    public void activarEdittext(final RadioGroup rg, final RadioButton si, final RadioButton no, final EditText et )
    {

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == si.getId() )
                {
                    et.setEnabled(true);
                    et.setVisibility(View.VISIBLE);
                    //Toast.makeText(FormularioActivity_cg3.this, "seleccionó si", Toast.LENGTH_SHORT).show();
                }

                else if (checkedId == no.getId() )
                {
                    et.setEnabled(false);
                    et.setVisibility(View.INVISIBLE);
                }
            }
        });


    }

    public void guardarDB3(View v) {

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,
                "administracion", null, 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        ContentValues registro = new ContentValues();



        registro.put("cedula", cedula);

        if (rb_diabetes_si.isChecked()==true)
        {
            registro.put("diabetes",1);
            registro.put("diabetes_t",et_diabetes.getText().toString());
        }
        else{
            registro.put("diabetes",0);
            registro.put("diabetes_t","NULL");
        }

        if (rb_hiper_si.isChecked()==true)
        {
            registro.put("hiper", 1);
            registro.put("hiper_t", et_hiper.getText().toString());
        }
        else
        {
            registro.put("hiper", 0);
            registro.put("hiper_t", "NULL");
        }

        if (rb_cancer_si.isChecked()==true)
        {
            registro.put("cancer", 1);
            registro.put("cancer_t", et_cancer.getText().toString());
        }
        else
        {
            registro.put("cancer", 0);
            registro.put("cancer_t", "NULL");
        }


        if(rb_epilepsia_si.isChecked()==true)
        {
            registro.put("epilepsia",1);
            registro.put("epilepsia_t", et_epilepsia.getText().toString());
        }else{
            registro.put("epilepsia", 0);
            registro.put("epilepsia_t", "NULL");
        }

        if(rb_quirur_si.isChecked()==true)
        {
            registro.put("quirur", 1);
            //registro.put("qiurur_t", et_quirur.getText().toString());
        }else{
            registro.put("quirur", 0);
            //registro.put("qiurur_t", "NULL");
        }

        if(rb_alergias_si.isChecked())
        {
            registro.put("aler",1);
            registro.put("aler_t", et_alergias.getText().toString());
        }else{
            registro.put("aler",0);
            registro.put("aler_t", "NULL");
        }

        if(rb_otros_si.isChecked())
        {
            registro.put("otros", 1);
            registro.put("otros_t", et_otros.getText().toString());
        }else{
            registro.put("otros", 0);
            registro.put("otros_t", "NULL");
        }


        // Sí existe el usuario para actualizar los datos

        Cursor fila = bd.rawQuery("select cedula from consultag_3T where cedula=" + cedula, null);

        if (fila.moveToFirst()) {


            int cant = bd.update("consultag_3T", registro, "cedula=" + cedula, null);
            Toast.makeText(this, "Actualización satisfactoria", Toast.LENGTH_SHORT).show();


        } else {


            // los inserto en la base de datos
            bd.insert("consultag_3T", null, registro);
            Toast.makeText(this, "Datos del usuario cargados", Toast.LENGTH_SHORT).show();


        }



        if (v.getId()==R.id.btn_guardar3)
        {

            Recursiva re = new Recursiva();
            re.guardarDB(getBaseContext(),cedula);


        }



            //-------------------------------------------------
            bd.close();


    }


    public void consulta(Integer cedula){

        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this,

                "administracion", null, (int) 1);

        SQLiteDatabase bd = admin.getWritableDatabase();

        Cursor fila = bd.rawQuery("select diabetes, diabetes_t,hiper, hiper_t, cancer, cancer_t, epilepsia, epilepsia_t,quirur,aler, aler_t,otros,otros_t from consultag_3T where cedula=" + cedula, null);
        Cursor fila1 = bd.rawQuery("select diabetes, diabetes_t,hiper, hiper_t, cancer, cancer_t, epilepsia, epilepsia_t,quirur,aler, aler_t,otros,otros_t from consultag_3 where cedula=" + cedula, null);


        if (fila.moveToFirst()) {

            if(fila.getInt(0) == 1)
            {
                rb_diabetes_si.setChecked(true);
                et_diabetes.setText(fila.getString(1));
            }
            else{
                rb_diabetes_no.setChecked(true);
            }
            if(fila.getInt(2) == 1)
            {
                rb_hiper_si.setChecked(true);
                et_hiper.setText(fila.getString(3));
            }
            else{
                rb_hiper_no.setChecked(true);
            }
            if(fila.getInt(4) == 1)
            {
                rb_cancer_si.setChecked(true);
                et_cancer.setText(fila.getString(5));
            }
            else{
                rb_cancer_no.setChecked(true);
            }
            if(fila.getInt(6) == 1)
            {
                rb_epilepsia_si.setChecked(true);
                et_epilepsia.setText(fila.getString(7));
            }
            else{
                rb_epilepsia_no.setChecked(true);
            }
            if(fila.getInt(8) == 1)
            {
                rb_quirur_si.setChecked(true);
                //et_quirur.setText(fila.getString(10));
            }
            else{
                rb_quirur_no.setChecked(true);
            }
            if(fila.getInt(9) == 1)
            {
                rb_alergias_si.setChecked(true);
                et_alergias.setText(fila.getString(10));
            }
            else{
                rb_alergias_no.setChecked(true);
            }
            if(fila.getInt(11) == 1)
            {
                rb_otros_si.setChecked(true);
                et_otros.setText(fila.getString(12));
            }
            else{
                rb_otros_no.setChecked(true);
            }

           // Toast.makeText(this, fila.getString(7), Toast.LENGTH_SHORT).show();


            habilitarTodo(false);
            btn_editar3.setEnabled(true);


        } else if(fila1.moveToFirst())
        {
            if(fila1.getInt(0) == 1)
            {
                rb_diabetes_si.setChecked(true);
                et_diabetes.setText(fila1.getString(1));
            }
            else{
                rb_diabetes_no.setChecked(true);
            }
            if(fila1.getInt(2) == 1)
            {
                rb_hiper_si.setChecked(true);
                et_hiper.setText(fila1.getString(3));
            }
            else{
                rb_hiper_no.setChecked(true);
            }
            if(fila1.getInt(4) == 1)
            {
                rb_cancer_si.setChecked(true);
                et_cancer.setText(fila1.getString(5));
            }
            else{
                rb_cancer_no.setChecked(true);
            }
            if(fila1.getInt(6) == 1)
            {
                rb_epilepsia_si.setChecked(true);
                et_epilepsia.setText(fila1.getString(7));
            }
            else{
                rb_epilepsia_no.setChecked(true);
            }
            if(fila1.getInt(8) == 1)
            {
                rb_quirur_si.setChecked(true);
                //et_quirur.setText(fila.getString(10));
            }
            else{
                rb_quirur_no.setChecked(true);
            }
            if(fila1.getInt(9) == 1)
            {
                rb_alergias_si.setChecked(true);
                et_alergias.setText(fila1.getString(10));
            }
            else{
                rb_alergias_no.setChecked(true);
            }
            if(fila1.getInt(11) == 1)
            {
                rb_otros_si.setChecked(true);
                et_otros.setText(fila1.getString(12));
            }
            else{
                rb_otros_no.setChecked(true);
            }

            // Toast.makeText(this, fila.getString(7), Toast.LENGTH_SHORT).show();


            habilitarTodo(false);
            btn_editar3.setEnabled(true);


        }else
        {
            btn_editar3.setEnabled(false);
            //Toast.makeText(this, "No existe ningún usuario con ese dni", Toast.LENGTH_SHORT).show();

        }


        bd.close();

    }


    public void habilitarTodo(boolean opcion) {


        rb_diabetes_si.setEnabled(opcion);
        rb_diabetes_no.setEnabled(opcion);
        et_diabetes.setEnabled(opcion);

        rb_hiper_si.setEnabled(opcion);
        rb_hiper_no.setEnabled(opcion);
        et_hiper.setEnabled(opcion);

        rb_cancer_si.setEnabled(opcion);
        rb_cancer_no.setEnabled(opcion);
        et_cancer.setEnabled(opcion);

        rb_epilepsia_si.setEnabled(opcion);
        rb_epilepsia_no.setEnabled(opcion);
        et_epilepsia.setEnabled(opcion);

        rb_quirur_si.setEnabled(opcion);
        rb_quirur_no.setEnabled(opcion);
        et_quirur.setEnabled(opcion);

        rb_alergias_si.setEnabled(opcion);
        rb_alergias_no.setEnabled(opcion);
        et_alergias.setEnabled(opcion);

        rb_otros_si.setEnabled(opcion);
        rb_otros_no.setEnabled(opcion);
        et_otros.setEnabled(opcion);


    }


}

