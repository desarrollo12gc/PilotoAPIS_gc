package com.grupocubo.desarrollo12.pilotoapis_gc.Activities.history;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.widget.Button;
import android.widget.ImageView;
import android.util.Log;
import android.Manifest;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.support.v4.content.ContextCompat;
import android.content.pm.PackageManager;

import com.grupocubo.desarrollo12.pilotoapis_gc.R;

public class CamaraActivity extends AppCompatActivity implements View.OnClickListener {


    private Button btn;
    private ImageView imagen;
    private Intent i;
    final static int cons =0;
    private Bitmap bmp;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camara);
        checkCameraPermission();
        init();
        // Set up the login form.
    }

    public void init(){
        btn = findViewById(R.id.btn_captura);
        btn.setOnClickListener(this);
        imagen = findViewById(R.id.imagen);
    }
    private void checkCameraPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.CAMERA);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.i("Mensaje", "No se tiene permiso para la camara!.");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 225);
        } else {
            Log.i("Mensaje", "Tienes permiso para usar la camara.");
        }
    }
    public void onClick(View v){
        int id;
        id=v.getId();
        switch (id){
            case R.id.btn_captura:
                i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(i,cons);
                break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if (resultCode==-1){
            Bundle ext = data.getExtras();
            bmp = (Bitmap)ext.get("data");
            imagen.setImageBitmap(bmp);
        }
    }
}
