package com.grupocubo.desarrollo12.pilotoapis_gc.Activities.history;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.grupocubo.desarrollo12.pilotoapis_gc.R;

public class MenuActivity extends AppCompatActivity {

    private ImageView btn_grillas;
    private ImageView btn_picasso;
    private ImageView btn_camara;
    private ImageView btn_gps;
    private ImageView btn_mapa;
    private ImageView btn_formulario;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        // Set up the login form.


        btn_grillas = (ImageView) findViewById(R.id.btn_grilla);
        btn_picasso = (ImageView) findViewById(R.id.btn_picasso);
        btn_camara = (ImageView) findViewById(R.id.btn_camara);
        btn_gps = (ImageView) findViewById(R.id.btn_gps);
        btn_mapa = (ImageView) findViewById(R.id.btn_mapa);
        btn_formulario = (ImageView) findViewById(R.id.btn_formulario);

        btn_grillas.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent ListSong = new Intent(MenuActivity.this, GrillasActivity.class);
                startActivity(ListSong);
            }

        });


        btn_picasso.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent ListSong = new Intent(MenuActivity.this, PicassoActivity.class);
                startActivity(ListSong);
            }

        });

        btn_camara.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent ListSong = new Intent(MenuActivity.this, CamaraActivity.class);
                startActivity(ListSong);
            }

        });

        btn_gps.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent ListSong = new Intent(MenuActivity.this, GpsActivity.class);
                startActivity(ListSong);
            }

        });


        btn_mapa.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent ListSong = new Intent(MenuActivity.this, MapaActivity.class);
                startActivity(ListSong);
            }

        });

        btn_formulario.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent ListSong = new Intent(MenuActivity.this, FormularioActivity_cg0.class);
                startActivity(ListSong);
            }

        });

    }

}
