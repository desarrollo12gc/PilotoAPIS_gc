package com.grupocubo.desarrollo12.pilotoapis_gc.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class AdminSQLiteOpenHelper extends SQLiteOpenHelper {

    private static final String NOMBRE_BASE_DATOS = "prueba_formularios.db";


    public AdminSQLiteOpenHelper(Context context, String nombre, SQLiteDatabase.CursorFactory factory, int version) {

        super(context, nombre, factory, version);

    }

    @Override

    public void onCreate(SQLiteDatabase db) {

        db.execSQL("" +
                "create table consultag_2("+
                    "cedula integer primary key," +
                    "peso integer," +
                    "talla integer, " +
                    "imc integer, " +
                    "temperatura integer," +
                    "tension integer," +
                    "FC integer," +
                    "FR integer," +
                    "BAI integer," +
                    "SR integer," +
                    "SP inte" +
                    "ger )");

        //aquí creamos la tabla de usuario ( )
        db.execSQL("" +
                "create table consultag_1(" +
                "cedula integer primary key," +
                "fecha text, " +
                "hora text, " +
                //"idCam text, " +
                "tipo_consulta text, " +
                "motivo text, " +
                "hallazgos text)");



        db.execSQL("create table consultag_3(" +
                "cedula integer primary key, " +
                "diabetes integer, " +
                "diabetes_t text, " +
                "hiper integer, " +
                "hiper_t text," +
                "cancer integer, " +
                "cancer_t text, " +
                "epilepsia integer, " +
                "epilepsia_t text," +
                "quirur integer, " +
                //"quirur_t text, " +
                "aler integer, " +
                "aler_t text," +
                "otros integer, " +
                "otros_t text)");

        db.execSQL("create table consultag_4(" +
                "cedula integer primary key, " +
                "cabeza text, " +
                "cardio text, " +
                "abdomen text," +
                "extremidades text," +
                "sistemaner text," +
                "sexo integer," +
                "planifica integer," +
                "metodo text," +
                "tiempo integer," +
                "dias integer," +
                "meses integer," +
                "años integer )");

        //-------------- TABLAS PARA GUARDAR TEMPORALES --------------------------\\

        db.execSQL("" +
                "create table consultag_2T("+
                "cedula integer primary key," +
                "peso integer," +
                "talla integer, " +
                "imc integer, " +
                "temperatura integer," +
                "tension integer," +
                "FC integer," +
                "FR integer," +
                "BAI integer," +
                "SR integer," +
                "SP inte" +
                "ger )");

        //aquí creamos la tabla de usuario ( )
        db.execSQL("" +
                "create table consultag_1T(" +
                "cedula integer primary key," +
                "fecha text, " +
                "hora text, " +
                //"idCam text, " +
                "tipo_consulta text, " +
                "motivo text, " +
                "hallazgos text)");



        db.execSQL("create table consultag_3T(" +
                "cedula integer primary key, " +
                "diabetes integer, " +
                "diabetes_t text, " +
                "hiper integer, " +
                "hiper_t text," +
                "cancer integer, " +
                "cancer_t text, " +
                "epilepsia integer, " +
                "epilepsia_t text," +
                "quirur integer, " +
                //"quirur_t text, " +
                "aler integer, " +
                "aler_t text," +
                "otros integer, " +
                "otros_t text)");

        db.execSQL("create table consultag_4T(" +
                "cedula integer primary key, " +
                "cabeza text, " +
                "cardio text, " +
                "abdomen text," +
                "extremidades text," +
                "sistemaner text," +
                "sexo integer," +
                "planifica integer," +
                "metodo text," +
                "tiempo integer," +
                "dias integer," +
                "meses integer," +
                "años integer )");



    }

    @Override

    public void onUpgrade(SQLiteDatabase db, int version1, int version2) {

       /* db.execSQL("drop table if exists consultag_1");
        db.execSQL("drop table if exists consultag_2");
        db.execSQL("drop table if exists consultag_3");
        db.execSQL("drop table if exists consultag_4");

        onCreate(db);*/

    }

}

